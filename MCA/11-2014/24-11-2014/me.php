<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>

class test
{
  public test($data1)
  {
     echo $data1;
  }
}

class test1 extends test
{
    public test($data1,$data2)
    {
       echo $data1.' '.$data2;
    }
}

$obj = new test1();
$obj->test('hello','world');
<br /><br /><br /><br /><br /><br />
class test{
    public function __call($name, $arguments)
    {
        if ($name === 'test'){
            if(count($arguments) === 1 ){
                return $this->test1($arguments[0]);
            }
            if(count($arguments) === 2){
                return $this->test2($arguments[0], $arguments[1]);
            }
        }
    }

    private function test1($data1)
    {
       echo $data1;
    }

    private function test2($data1,$data2)
    {
       echo $data1.' '.$data2;
    }
}

$test = new test();
$test->test('one argument'); //echoes "one argument"
$test->test('two','arguments'); //echoes "two arguments"

<br /><br /><br /><br /><br /><br />

class A {
 
    public function __call($method_name,$arguments) {
 
        $methodArray = array('func','func1','func2');
 
        if (in_array($method_name,$methodArray) === false) {
            die("\n Method does not exist");
        }
 
        if (count($arguments) === 2) {
            $this->func($arguments[0],$arguments[1]);
        }
        elseif (count($arguments) === 1) {
            $this->func($arguments[0]);
        }
        elseif (count($arguments) === 0) {
            $this->func();
        }
        else {
            echo "\n unknown method";
            return false;
        }
    }
    function func($a = null,$b = null) {
        echo "\n <br/> from function func, arguments = $a $b";
    }
 
    function func1($a = null) {
        echo "\n <br/> from function func1";
    }
 
    function func2($a = null,$b = null) {
        echo "\n <br/> from function func2";
    }
 
} //
 
$objA = new A;
$objA->func('a');
$objA->func('a','b');
$objA->func('c');
$objA->func();

</body>
</html>
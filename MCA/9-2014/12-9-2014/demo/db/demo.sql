-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 09, 2015 at 12:37 PM
-- Server version: 5.5.24-log
-- PHP Version: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `demo`
--

-- --------------------------------------------------------

--
-- Table structure for table `registration`
--

CREATE TABLE IF NOT EXISTS `registration` (
  `RegId` int(11) NOT NULL AUTO_INCREMENT,
  `RegDate` date NOT NULL,
  `RegEmail` varchar(30) NOT NULL,
  `RegPassword` varchar(16) NOT NULL,
  `RegType` char(1) NOT NULL,
  `RegStatus` tinyint(1) NOT NULL,
  `RegName` varchar(30) NOT NULL,
  `RegAddress` varchar(150) NOT NULL,
  `RegCity` varchar(20) NOT NULL,
  `RegContact` varchar(13) NOT NULL,
  `RegPhoto` varchar(50) NOT NULL,
  PRIMARY KEY (`RegId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `registration`
--

INSERT INTO `registration` (`RegId`, `RegDate`, `RegEmail`, `RegPassword`, `RegType`, `RegStatus`, `RegName`, `RegAddress`, `RegCity`, `RegContact`, `RegPhoto`) VALUES
(2, '2015-09-08', 'abc@yahoo.com', '123456', 'A', 1, 'Core Computer', 'Near Dhaval Plaza', 'Kadi', '7383005050', '1.jpg'),
(3, '2015-09-08', 'core@yahoo.com', '123456', 'M', 1, 'Jayesh Patel', 'near Railway Station ', '', '99865321', 'Koala.jpg'),
(5, '2015-09-09', 'xyz@yahoo.com', '123456', 'M', 1, 'Manish Prajapati', 'Kadi', 'Kalol', '4545454', 'Tulips.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE IF NOT EXISTS `student` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `city` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id`, `name`, `city`) VALUES
(1, 'Jayesh', 'Kadi'),
(2, 'Mahesh', 'Kalol');

-- --------------------------------------------------------

--
-- Table structure for table `test`
--

CREATE TABLE IF NOT EXISTS `test` (
  `id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `test`
--

INSERT INTO `test` (`id`, `name`) VALUES
(1, 'hjhjh'),
(2147483647, '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

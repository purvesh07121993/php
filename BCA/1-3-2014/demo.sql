-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 03, 2014 at 12:55 PM
-- Server version: 5.5.24-log
-- PHP Version: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `demo`
--

-- --------------------------------------------------------

--
-- Table structure for table `profile`
--

CREATE TABLE IF NOT EXISTS `profile` (
  `ProId` int(11) NOT NULL AUTO_INCREMENT,
  `RegId` int(11) NOT NULL,
  `ProName` varchar(20) NOT NULL,
  `ProAddress` varchar(50) NOT NULL,
  `ProCity` varchar(20) NOT NULL,
  `ProDob` date NOT NULL,
  `ProGender` varchar(8) NOT NULL,
  PRIMARY KEY (`ProId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `profile`
--

INSERT INTO `profile` (`ProId`, `RegId`, `ProName`, `ProAddress`, `ProCity`, `ProDob`, `ProGender`) VALUES
(1, 2, 'imran Kureshi', 'DHaval Plaza', 'Ahmedabad', '0000-00-00', '1993-1-1'),
(2, 3, 'Core Computer', 'DHaval Plaza', 'Kadi', '1999-01-01', 'Male');

-- --------------------------------------------------------

--
-- Table structure for table `registration`
--

CREATE TABLE IF NOT EXISTS `registration` (
  `RegId` int(11) NOT NULL AUTO_INCREMENT,
  `RegDate` date NOT NULL,
  `RegEmail` varchar(30) NOT NULL,
  `RegPassword` varchar(15) NOT NULL,
  `RegType` varchar(8) NOT NULL,
  `RegStatus` varchar(8) NOT NULL,
  PRIMARY KEY (`RegId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `registration`
--

INSERT INTO `registration` (`RegId`, `RegDate`, `RegEmail`, `RegPassword`, `RegType`, `RegStatus`) VALUES
(1, '2014-01-03', 'imran@yahoo.com', '123', 'Member', 'Active'),
(2, '2014-01-03', 'kureshi@yahoo.com', '123456', 'Member', 'Active'),
(3, '2014-01-03', 'abc@yahoo.com', '444', 'Member', 'Active');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
